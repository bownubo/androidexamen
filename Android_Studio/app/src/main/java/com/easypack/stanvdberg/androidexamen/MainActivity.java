package com.easypack.stanvdberg.androidexamen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AttractionListAdapter.ItemClickListener{
    AttractionListAdapter adapter;
    String type;
    ArrayList<String> AttractionList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        // data to populate the RecyclerView with
        AttractionList = GlobalFunctions.getAttractionList(this);

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvAttractionName);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AttractionListAdapter(this, AttractionList);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent (this, AttractionDetail.class);
        intent.putExtra("type", type);
        intent.putExtra("Attraction", AttractionList.get(position));
        startActivity(intent);
    }
}
