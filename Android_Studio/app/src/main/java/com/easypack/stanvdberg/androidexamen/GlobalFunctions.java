package com.easypack.stanvdberg.androidexamen;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class GlobalFunctions {
    Context mcontext;

    public GlobalFunctions(Context context) {
        this.mcontext = context;
    }


    public static String loadJSONFromAsset(Context context, int fileName) {
        String json = null;
        try {
            InputStream is = context.getResources().openRawResource(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static ArrayList<String> getAttractionList(Context context) {
        ArrayList<String> catKeyStrings = new ArrayList<>();

        try {
            String jsonString = GlobalFunctions.loadJSONFromAsset(context, R.raw.data);
            JSONObject json = new JSONObject(jsonString);
            JSONObject attractionData = json.getJSONObject("attraction");
            Iterator attractionKeys = attractionData.keys();

            while (attractionKeys.hasNext()) {
                String key = (String) attractionKeys.next();
                catKeyStrings.add(key);
            }

            return catKeyStrings;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
