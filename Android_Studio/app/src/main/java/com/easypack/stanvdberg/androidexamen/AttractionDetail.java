package com.easypack.stanvdberg.androidexamen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AttractionDetail extends AppCompatActivity {
    static Context context;
    JSONArray jsonAttractionData;
    Intent intent;
    String attraction;
    String attractionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attractiondetail);
        context = getApplicationContext();
        intent = getIntent();
        attraction = intent.getStringExtra("Attraction");
        setData();

    }

    public void GetJsonData() {
            try {
                String jsonString = GlobalFunctions.loadJSONFromAsset(context, R.raw.data);
                JSONObject json = new JSONObject(jsonString);
                jsonAttractionData = json.getJSONObject("Attraction").getJSONArray(this.attraction);

                } catch (Exception e) {
                    e.printStackTrace();
                }
    }


    public void setData() {
        GetJsonData();
        try {
            jsonAttractionData.toString();
            attractionName = jsonAttractionData.getString(0);
            Log.e("attraction", attractionName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
